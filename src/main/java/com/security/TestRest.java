package com.security;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
/**
 *
 * @author MELOIFI
 *
 */
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin("10.104.102.186")
@RestController
@RequestMapping(value = "/")
public class TestRest {
	private static final Logger logger =  LoggerFactory.getLogger(TestRest.class);
	@GetMapping(value = "/test")
	public String test() {
			return "test";
	}
}